using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class CreateFolder : MonoBehaviour
{
    public InputField folderNameInput; // Reference to an InputField where the user can enter the folder name.
    private void Start()
    {
        CreateeFolder();
    }
    public void CreateeFolder()
    {
        string folderName = folderNameInput.text.Trim();

        if (string.IsNullOrEmpty(folderName))
        {
            Debug.LogError("Folder name cannot be empty.");
            return;
        }

        string folderPath = Path.Combine(Application.dataPath, folderName);

        if (!Directory.Exists(folderPath))
        {
            Directory.CreateDirectory(folderPath);
            Debug.Log("Folder created at: " + folderPath);
        }
        else
        {
            Debug.Log("Folder already exists at: " + folderPath);
        }
    }
}
