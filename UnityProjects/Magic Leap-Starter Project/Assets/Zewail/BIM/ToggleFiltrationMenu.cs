using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleFiltrationMenu : MonoBehaviour
{
    [SerializeField] GameObject menuToToggle;
    [SerializeField] RectTransform moverObject;

    [SerializeField] Vector3 moverObjectNewLocalPosition;
    [SerializeField] float moverObjectNewWidth;
    [SerializeField] float moverObjectNewHeight;

    Vector3 moverObjectOriginalLocalPosition;
    float moverObjectOriginalWidth;
    float moverObjectOriginalHeight;
    
    private void Awake()
    {
        moverObjectOriginalLocalPosition = moverObject.localPosition;
        moverObjectOriginalWidth = moverObject.rect.width;
        moverObjectOriginalHeight = moverObject.rect.height;
    }

    public void ToggleMenu()
    {
        menuToToggle.SetActive(!menuToToggle.activeInHierarchy);

        if (!menuToToggle.activeInHierarchy)
        {
            moverObject.localPosition = moverObjectNewLocalPosition;
            moverObject.sizeDelta = new(moverObjectNewWidth, moverObjectNewHeight);
        }
        else
        {
            moverObject.localPosition = moverObjectOriginalLocalPosition;
            moverObject.sizeDelta = new(moverObjectOriginalWidth, moverObjectOriginalHeight);
        }
    }
}
