using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    [SerializeField] string levelName;

    public void LoadLevel()
    {
        SceneManager.LoadSceneAsync(levelName);
    }
}
