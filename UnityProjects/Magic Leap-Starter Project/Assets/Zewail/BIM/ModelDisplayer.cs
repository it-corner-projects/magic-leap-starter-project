using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelDisplayer : MonoBehaviour
{
    [Header("Initializing location and rotation")]
    [SerializeField] GameObject cameraGameObject;
    [SerializeField] Vector3 positionRelativeToCamera;
    [SerializeField] Vector3 rotationRelativeToCamera;
    [SerializeField] bool reverseForwardDirectionOnStart;

    [SerializeField] List<ModelButton> modelButtons;

    private void Awake()
    {
        if (cameraGameObject != null)
        {
            transform.position = cameraGameObject.transform.position + positionRelativeToCamera.x * cameraGameObject.transform.right
                + positionRelativeToCamera.y * cameraGameObject.transform.up
                + positionRelativeToCamera.z * cameraGameObject.transform.forward;

            Quaternion lookAtRotation = Quaternion.LookRotation(cameraGameObject.transform.position - transform.position);

            Vector3 lookAtRotation_EulerAngles = lookAtRotation.eulerAngles;
            float yawRotation = reverseForwardDirectionOnStart ? lookAtRotation_EulerAngles.y - 180 : lookAtRotation_EulerAngles.y;

            //Rotate to face the camera then rotate the number of degrees specified in the "rotation relative to camera" variable around the new local axes.
            transform.rotation = Quaternion.Euler(lookAtRotation_EulerAngles.x, yawRotation, lookAtRotation_EulerAngles.z) * Quaternion.Euler(rotationRelativeToCamera);
        }

        ShowAssembledModel();
    }
    private void OnEnable()
    {
        modelButtons.ForEach(x => x.OnModelDisplayed += ActivateModel);
    }
    private void OnDisable()
    {
        modelButtons.ForEach(x => x.OnModelDisplayed -= ActivateModel);
    }

    public void ShowAssembledModel()
    {
        modelButtons.ForEach(model => model.ShowModel(false));
    }

    void ActivateModel(ModelButton model)
    {
        modelButtons.ForEach(x =>
        {
            if (x == model)
            {
                x.ShowModel(false);
            }
            else
            {
                x.HideModel();
            }
        });
    }
}
