using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasPositionShifter : MonoBehaviour
{
    [Tooltip("The forwards distance from the camera that this object should be placed.")]
    [SerializeField] float CanvasDistanceForwards = 1.5f;

    [Tooltip("The upwards distance from the camera that this object should be placed.")]
    [SerializeField] float CanvasDistanceUpwards = 0.0f;

    [Tooltip("The upwards distance from the camera that this object should be placed.")]
    [SerializeField] float CanvasDistanceSide = 0.0f;

    [Tooltip("The speed at which this object changes its position.")]
    [SerializeField] float PositionLerpSpeed = 5f;

    [Tooltip("The speed at which this object changes its rotation.")]
    [SerializeField] float RotationLerpSpeed = 5f;

    
    // The camera this object will be in front of.
    private Camera _camera;

    /// <summary>
    /// Initializes variables and verifies that necessary components exist.
    /// </summary>
    void Awake()
    {
        _camera = Camera.main;
    }

    Vector3 fromCameraToCanvas;

    private void OnEnable()
    {
        fromCameraToCanvas = transform.position - _camera.transform.position;
    }

    private void OnDisable()
    {
        fromCameraToCanvas = Vector3.zero;
    }

    /// <summary>
    /// Update position and rotation of this canvas object to face the camera using lerp for smoothness.
    /// </summary>
    void Update()
    {
        #region Old Logic
        //// Move the object CanvasDistance units in front of the camera.
        //float posSpeed = Time.deltaTime * PositionLerpSpeed;
        //Vector3 posTo = _camera.transform.position + (_camera.transform.forward * CanvasDistanceForwards)
        //    + (_camera.transform.up * CanvasDistanceUpwards)
        //    + (_camera.transform.right * CanvasDistanceSide);

        //calculatedPosition = posTo;

        //transform.position = Vector3.SlerpUnclamped(transform.position, posTo, posSpeed);

        //// Rotate the object to face the camera.
        //float rotSpeed = Time.deltaTime * RotationLerpSpeed;
        //Quaternion rotTo = Quaternion.LookRotation(transform.position - _camera.transform.position);
        //transform.rotation = Quaternion.Slerp(transform.rotation, rotTo, rotSpeed);
        #endregion

        #region New Logic
        // Move the object units in front of the camera.
        float posSpeed = Time.deltaTime * PositionLerpSpeed;
        Vector3 posTo = _camera.transform.position + (_camera.transform.forward * fromCameraToCanvas.z)
            + (_camera.transform.up * fromCameraToCanvas.y)
            + (_camera.transform.right * fromCameraToCanvas.x);

        transform.position = Vector3.Slerp(transform.position, posTo, posSpeed);

        // Rotate the object to face the camera.
        float rotSpeed = Time.deltaTime * RotationLerpSpeed;
        Quaternion rotTo = Quaternion.LookRotation(transform.position - _camera.transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotTo, rotSpeed);
        #endregion

    }
}
