using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BIMModelsToggle : MonoBehaviour
{
    [SerializeField] GameObject buildingStructure;
    [SerializeField] GameObject stairs;
    [SerializeField] GameObject windowsAndDoors;
    [SerializeField] GameObject facade;

    private void Awake()
    {
        ShowAssembled();
    }

    public void ShowAssembled()
    {
        ActivateAllModels();
    }

    public void ShowStairs()
    {
        ActivateModel(stairs);
    }

    public void ShowBuildingStructure()
    {
        ActivateModel(buildingStructure);
    }

    public void ShowWindowsAndDoors()
    {
        ActivateModel(windowsAndDoors);
    }
    public void ShowFacade()
    {
        ActivateModel(facade);
    }

    void ActivateModel(GameObject model)
    {
        buildingStructure.SetActive(model == buildingStructure);
        stairs.SetActive(model == stairs);
        windowsAndDoors.SetActive(model == windowsAndDoors);
        facade.SetActive(model == facade);
    }

    void ActivateAllModels()
    {
        buildingStructure.SetActive(true);
        stairs.SetActive(true);
        facade.SetActive(true);
        windowsAndDoors.SetActive(true);
    }
}
