using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCameraButton : MonoBehaviour
{
    [SerializeField] GameObject canvasRoot;
    [SerializeField] CanvasPositionShifter uiCameraFollowScript;

    Camera _camera;

    private void Awake()
    {
        uiCameraFollowScript.enabled = false;

        _camera = Camera.main;
    }

    public void ToggleFollowing()
    {
        uiCameraFollowScript.enabled = !uiCameraFollowScript.enabled;

        if(!uiCameraFollowScript.enabled)
        {
            canvasRoot.transform.position = transform.position;

            uiCameraFollowScript.transform.localPosition = Vector3.zero;

            uiCameraFollowScript.transform.localRotation = Quaternion.identity;

            canvasRoot.transform.rotation = Quaternion.LookRotation(canvasRoot.transform.position - _camera.transform.position);
        }
    }

}
