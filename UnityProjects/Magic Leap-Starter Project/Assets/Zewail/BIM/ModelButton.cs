using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelButton : MonoBehaviour
{
    public Action<ModelButton> OnModelDisplayed;
    [SerializeField] GameObject model;

    public void ShowModel(bool invokeTheModelDisplayed)
    {
        model.SetActive(true);

        if(invokeTheModelDisplayed)
        {
            OnModelDisplayed?.Invoke(this);
        }
    }

    public void HideModel()
    {
        model.SetActive(false);
    }
}
